/**
* This tests the BDD functionality in TestBox. This is CF10+, Railo4+
*/
component extends="testbox.system.BaseSpec"{

/*********************************** LIFE CYCLE Methods ***********************************/

	function beforeAll(){
		application.mockBox = getMockBox();
		application.hyper = application.mockBox.createMock( "hyper.HyperRequest" );
		
		// setup a fake http client
		application.httpStub = application.mockBox.createStub( implements="hyper.HyperHttpClientInterface" );
		application.hyper.setHttpClient( application.httpStub );

		application.Stocky = new models.Stocky()
				.setHyper( application.hyper );
	}

	function afterAll(){
		structClear( application );
	}

/*********************************** BDD SUITES ***********************************/

	function run(){

		/** 
		* describe() starts a suite group of spec tests.
		* Arguments:
		* @title The title of the suite, Usually how you want to name the desired behavior
		* @body A closure that will resemble the tests to execute.
		* @labels The list or array of labels this suite group belongs to
		* @asyncAll If you want to parallelize the execution of the defined specs in this suite group.
		* @skip A flag that tells TestBox to skip this suite group from testing if true
		*/
		describe( "cfStocky", function(){

			beforeEach( function() {
				// we create a fake response object which is ultimately ...
				// sent back to cfStocky with the response data.
				var response = new hyper.HyperResponse(
					originalRequest = application.hyper,
					statusCode = 200,
					headers = {},
					data = '{"type":"dummyJSON"}'
				);
				application.httpStub.$( "send", response );
			} );

			
			/** 
			* it() describes a spec to test. Usually the title is prefixed with the suite name to create an expression.
			* Arguments:
			* @title The title of the spec
			* @spec A closure that represents the test to execute
			* @labels The list or array of labels this spec belongs to
			* @skip A flag that tells TestBox to skip this spec from testing if true
			*/
			it("can use base URL", function(){
				application.Stocky.setBaseURL( "http://mywebsite.com" );
				expect( application.stocky.getBaseURL() ).toBe( "http://mywebsite.com" );
				expect( application.stocky.getHyper().getBaseURL() ).toBe( "http://mywebsite.com" );
			});

			it("can get company profile", function() {
				var res = application.Stocky.getCompanyProfile( "MSFT" );

				expect( application.stocky.getHyper().getURL() ).toBe( "/company/profile/MSFT" );
				expect( res ).toBeTypeOf( "component" );
				expect( res.json() ).toBeTypeOf( "struct" );
			});

			it("can get company rating", function() {
				var res = application.Stocky.getCompanyRating( "AAPL" );

				expect( application.stocky.getHyper().getURL() ).toBe( "/company/rating/AAPL" );
				expect( res ).toBeTypeOf( "component" );
				expect( res.json() ).toBeTypeOf( "struct" );
			});

			it("can get financial ratios", function() {
				var res = application.Stocky.getFinancialRatios( "AAPL" );

				expect( application.stocky.getHyper().getURL() ).toBe( "/financial-ratios/AAPL" );
				expect( res ).toBeTypeOf( "component" );
				expect( res.json() ).toBeTypeOf( "struct" );
			});

			it("can get realtime price per stock", function() {
				var res = application.Stocky.getRealtimePrice( "AAPL" );

				expect( application.stocky.getHyper().getURL() ).toBe( "/stock/real-time-price/AAPL" );
				expect( res ).toBeTypeOf( "component" );
				expect( res.json() ).toBeTypeOf( "struct" );
			});

			it("can get realtime price for all stocks", function() {
				var res = application.Stocky.getRealtimePrice();

				expect( application.stocky.getHyper().getURL() ).toBe( "/stock/real-time-price" );
				expect( res ).toBeTypeOf( "component" );
				expect( res.json() ).toBeTypeOf( "struct" );
			});

			it("can get stock symbol list", function() {
				var res = application.Stocky.getStockSymbols();

				expect( application.stocky.getHyper().getURL() ).toBe( "/company/stock/list" );
				expect( res ).toBeTypeOf( "component" );
				expect( res.json() ).toBeTypeOf( "struct" );
			});

			it("can get major index price list", function() {
				var res = application.Stocky.getMajorIndex( );

				expect( application.stocky.getHyper().getURL() ).toBe( "/majors-indexes" );
				expect( res ).toBeTypeOf( "component" );
				expect( res.json() ).toBeTypeOf( "struct" );
			});

			it("can get single index price", function() {
				var res = application.Stocky.getMajorIndex( ".DJI" );

				expect( application.stocky.getHyper().getURL() ).toBe( "/majors-indexes/.DJI" );
				expect( res ).toBeTypeOf( "component" );
				expect( res.json() ).toBeTypeOf( "struct" );
			});

			it("can get most active stocks", function() {
				var res = application.Stocky.getActiveStocks();

				expect( application.stocky.getHyper().getURL() ).toBe( "/stock/actives" );
				expect( res ).toBeTypeOf( "component" );
				expect( res.json() ).toBeTypeOf( "struct" );
			});

			it("can get most gained stocks", function() {
				var res = application.Stocky.getMostGainedStocks();

				expect( application.stocky.getHyper().getURL() ).toBe( "/stock/gainers" );
				expect( res ).toBeTypeOf( "component" );
				expect( res.json() ).toBeTypeOf( "struct" );
			});

			it("can get most lost stocks", function() {
				var res = application.Stocky.getMostLostStocks();

				expect( application.stocky.getHyper().getURL() ).toBe( "/stock/losers" );
				expect( res ).toBeTypeOf( "component" );
				expect( res.json() ).toBeTypeOf( "struct" );
			});

			it("can get NYSE market trading hours", function() {
				var res = application.Stocky.getNYSEMarketHours();

				expect( application.stocky.getHyper().getURL() ).toBe( "/is-the-market-open" );
				expect( res ).toBeTypeOf( "component" );
				expect( res.json() ).toBeTypeOf( "struct" );
			});

			it("can get sector performance list", function() {
				var res = application.Stocky.getSectorPerformance();

				expect( application.stocky.getHyper().getURL() ).toBe( "/stock/sectors-performance" );
				expect( res ).toBeTypeOf( "component" );
				expect( res.json() ).toBeTypeOf( "struct" );
			});

			/**
			 * The following methods need to also test the `period` argument!
			*/

			it("can get financial statement", function() {
				var res = application.Stocky.getIncomeStatement( "MSFT" );

				expect( application.stocky.getHyper().getURL() ).toBe( "/financials/income-statement/MSFT" );
				expect( res ).toBeTypeOf( "component" );
				expect( res.json() ).toBeTypeOf( "struct" );
			});

			it("can get balance sheet", function() {
				var res = application.Stocky.getBalanceSheet( "MSFT" );

				expect( application.stocky.getHyper().getURL() ).toBe( "/financials/balance-sheet-statement/MSFT" );
				expect( res ).toBeTypeOf( "component" );
				expect( res.json() ).toBeTypeOf( "struct" );
			});

			it("can get enterprise value", function() {
				var res = application.Stocky.getEnterpriseValue( "AAPL" );

				expect( application.stocky.getHyper().getURL() ).toBe( "/enterprise-value/AAPL" );
				expect( res ).toBeTypeOf( "component" );
				expect( res.json() ).toBeTypeOf( "struct" );
			});

			it("can get key metrics", function() {
				var res = application.Stocky.getKeyMetrics( "AAPL" );

				expect( application.stocky.getHyper().getURL() ).toBe( "/company-key-metrics/AAPL" );
				expect( res ).toBeTypeOf( "component" );
				expect( res.json() ).toBeTypeOf( "struct" );
			});

			it("can get financial growth", function() {
				var res = application.Stocky.getFinancialGrowth( "AAPL" );

				expect( application.stocky.getHyper().getURL() ).toBe( "/financial-statement-growth/AAPL" );
				expect( res ).toBeTypeOf( "component" );
				expect( res.json() ).toBeTypeOf( "struct" );
			});

			it("can get cash flow", function() {
				var res = application.Stocky.getCashFlow( "AAPL" );

				expect( application.stocky.getHyper().getURL() ).toBe( "/financials/cash-flow-statement/AAPL" );
				expect( res ).toBeTypeOf( "component" );
				expect( res.json() ).toBeTypeOf( "struct" );
			});

			it("can get discounted cash flow", function() {
				var res = application.Stocky.getDiscountedCashFlow( "AAPL" );

				expect( application.stocky.getHyper().getURL() ).toBe( "/company/discounted-cash-flow/AAPL" );
				expect( res ).toBeTypeOf( "component" );
				expect( res.json() ).toBeTypeOf( "struct" );
			});

			it("can get historical discounted cash flow", function() {
				var res = application.Stocky.getHistoricalDiscountedCashFlow( "AAPL" );

				expect( application.stocky.getHyper().getURL() ).toBe( "/company/historical-discounted-cash-flow/AAPL" );
				expect( res ).toBeTypeOf( "component" );
				expect( res.json() ).toBeTypeOf( "struct" );
			});
			
		});


	}
}