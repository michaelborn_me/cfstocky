# cfStocky

![cfmlbadges](https://cfmlbadges.monkehworks.com/images/badges/made-with-cfml.svg) ![cfmlbadges](https://cfmlbadges.monkehworks.com/images/badges/tested-with-testbox.svg) ![Adobe CF11 Compatible](https://cfmlbadges.monkehworks.com/images/badges/compatibility-coldfusion-11.svg) ![Adobe CF2016 Compatible](https://cfmlbadges.monkehworks.com/images/badges/compatibility-coldfusion-2016.svg) ![Lucee 5 compatible](https://cfmlbadges.monkehworks.com/images/badges/compatibility-lucee-5.svg)

A CFML wrapper for the [Financial Modeling Prep API](https://financialmodelingprep.com/developer/docs/). Get stock prices and company fundamentals via a simple, clean API.

[![Bitbucket Pipelines build status](https://img.shields.io/bitbucket/pipelines/michaelborn_me/cfstocky.svg?label=build&style=flat-square)](https://bitbucket.org/michaelborn_me/cfstocky/addon/pipelines/home#!/results/branch/master/page/1)

## Getting Started

1. Install this module - `box install cfstocky`
2. OPTIONAL: Configure base api URL (if changed) via `moduleSettings.cfstocky.baseURL` in `config/Coldbox.cfc`:

```js
moduleSettings = {
	cfStocky : {
		baseURL : "https://financialmodelingprep.com/api/v3/"
	}
}
```

3. Inject the cfstocky service:

```js
property name="stocky" inject="Stocky@cfStocky";
```

## API Methods

* Most API methods require a ticker symbol ( like `MSFT` or `GM`) as the first argument.
* Each API method returns a `HyperResponse` object from [hyper](https://forgebox.io/view/hyper). You can call `.getData()`, `.getStatusCode()`, `.isSuccess()`, `.isError()`, and `.json()` to return JSON de-serialized as a struct.

### Get Company Profile

```js
var data = Stocky.getCompanyProfile( "MSFT" ).json();
```

https://financialmodelingprep.com/developer/docs/companies-key-stats-free-api/

### Financial Statements

https://financialmodelingprep.com/developer/docs/financial-statement-free-api/

#### Get Income Statement

```js
var data = Stocky.getIncomeStatement( "MSFT" ).json();
```

https://financialmodelingprep.com/developer/docs/financial-statement-free-api/#Income-Statement

#### Get Balance Sheet

```js
var data = Stocky.getBalanceSheet( "MSFT" ).json();
```

https://financialmodelingprep.com/developer/docs/financial-statement-free-api/#Balance-Sheet-Statement

#### Get Cash Flow Statement

```js
var data = Stocky.getCashFlow( "MSFT" ).json();
```

https://financialmodelingprep.com/developer/docs/financial-statement-free-api/#Cash-Flow-Statement

### Get Financial Ratios

```js
var data = Stocky.getFinancialRatios( "MSFT" ).json();
```

https://financialmodelingprep.com/developer/docs/financial-ratio-free-api/

### Get Financial Ratios

```js
var data = Stocky.getFinancialRatios( "MSFT" ).json();
```

https://financialmodelingprep.com/developer/docs/financial-ratio-free-api/

### Get Enterprise Value

```js
var data = Stocky.getEnterpriseValue( "MSFT" ).json();
```

https://financialmodelingprep.com/developer/docs/company-enterprise-value-api/

### Get Key Metrics

```js
var data = Stocky.getKeyMetrics( "MSFT" ).json();
```

https://financialmodelingprep.com/developer/docs/company-key-metrics-api/

### Get Financial Growth

```js
var data = Stocky.getFinancialGrowth( "MSFT" ).json();
```

https://financialmodelingprep.com/developer/docs/company-enterprise-value-api/

### Get Company Rating

```js
var data = Stocky.getCompanyRating( "MSFT" ).json();
```

https://financialmodelingprep.com/developer/docs/companies-rating-free-api/

### Discounted Cash Flow

#### Get Discounted Cash Flow

```js
var data = Stocky.getDiscountedCashFlow( "MSFT" ).json();
```

https://financialmodelingprep.com/developer/docs/companies-dcf-reports-free-api/#DCF

#### Get Historical Discounted Cash Flow

```js
var data = Stocky.getHistoricalDiscountedCashFlow( "MSFT" ).json();
```

https://financialmodelingprep.com/developer/docs/companies-dcf-reports-free-api/#Historical-DCF

### Stock Prices

#### Get Stock Real Time Price

```js
var data = Stocky.getRealtimePrice( "MSFT" ).json();
```

https://financialmodelingprep.com/developer/docs/realtime-stock-quote-api/#Stock-Realtime-Price

#### Get Price List for All Stocks

```js
var data = Stocky.getRealtimePrice().json();
```

https://financialmodelingprep.com/developer/docs/realtime-stock-quote-api/#Stock-Price-list

### Get All Stock Symbols

```js
var data = Stocky.getStockSymbols().json();
```

https://financialmodelingprep.com/developer/docs/stock-market-quote-free-api/

### Stock Market

#### Get All Major Stock Indexes

```js
var data = Stocky.getMajorIndexPriceList().json();
```

https://financialmodelingprep.com/developer/docs/indexes-in-stock-market-free-api/#Majors-Indexes-List

#### Get Single Index Price

```js
var data = Stocky.getMajorIndexPriceList( ".DJI" ).json();
```

https://financialmodelingprep.com/developer/docs/indexes-in-stock-market-free-api/#Majors-Indexes-List

#### Get Most Active Stocks

```js
var data = Stocky.getActiveStocks().json();
```

https://financialmodelingprep.com/developer/docs/most-actives-stock-market-data-free-api/

#### Get Most Gainer Stocks

```js
var data = Stocky.getMostGainedStocks().json();
```

https://financialmodelingprep.com/developer/docs/most-gainers-stock-market-data-free-api/

#### Get Most Loser Stocks

```js
var data = Stocky.getMostLostStocks().json();
```

https://financialmodelingprep.com/developer/docs/most-losers-stock-market-data-free-api/

#### Get NYSE Market Hours

Find out if the markets are open or closed today.

```js
var data = Stocky.getNYSEMarketHours().json();
```

https://financialmodelingprep.com/developer/docs/is-the-market-open/

#### Get NYSE Market Hours

Find out if the markets are open or closed today.

```js
var data = Stocky.getNYSEMarketHours().json();
```

#### Get Sector Performance

See the change in performance for each market sector.

```js
var data = Stocky.getSectorPerformanceList().json();
```

https://financialmodelingprep.com/developer/docs/is-the-market-open/

## TODO

* Add method for batch request stock historical price API
* Add methods for historical price API
* Add methods for batch request realtime price API
* Add methods for cryptocurrencies and forex APIs
* Improve documentation with response data screenshots or data tables.

## The Good News

> For all have sinned, and come short of the glory of God ([Romans 3:23](https://www.kingjamesbibleonline.org/Romans-3-23/))

> But God commendeth his love toward us, in that, while we were yet sinners, Christ died for us. ([Romans 5:8](https://www.kingjamesbibleonline.org/Romans-5-8))

> That if thou shalt confess with thy mouth the Lord Jesus, and shalt believe in thine heart that God hath raised him from the dead, thou shalt be saved. ([Romans 10:9](https://www.kingjamesbibleonline.org/Romans-10-9/))
