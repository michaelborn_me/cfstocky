component extends="BaseAPI"{

	public function getCompanyProfile( required string ticker ){
		var endpoint = "/company/profile/" & arguments.ticker;
		return getHyper().get( endpoint );
	}

	public function getFinancialRatios( required string ticker ){
		var endpoint = "/financial-ratios/" & arguments.ticker;
		return getHyper().get( url = endpoint );
	}

	public function getCompanyRating( required string ticker ){
		var endpoint = "/company/rating/" & arguments.ticker;
		return getHyper().get( url = endpoint );
	}

	public function getCashFlow( required string ticker ){
		var endpoint = "/financials/cash-flow-statement/" & arguments.ticker;
		return getHyper().get( url = endpoint );
	}

	public function getDiscountedCashFlow( required string ticker ){
		var endpoint = "/company/discounted-cash-flow/" & arguments.ticker;
		return getHyper().get( url = endpoint );
	}

	public function getHistoricalDiscountedCashFlow( required string ticker, string period = "annual" ){
		var endpoint = "/company/historical-discounted-cash-flow/" & arguments.ticker;
		var validPeriods = ["annual","quarter"];
		if ( !validPeriods.find( arguments.period ) ){
			throw( "Invalid period; must be one of #validPeriods.toList()#" );
		}
		var params = {
			"period" : arguments.period
		};
		return getHyper().get( url = endpoint, queryParams = params );
	}

	public function getRealtimePrice( string ticker = "" ){
		var endpoint = "/stock/real-time-price";
		if ( arguments.ticker > "" ){
			endpoint &= "/#arguments.ticker#";
		}
		return getHyper().get( url = endpoint );
	}

	public function getStockSymbols( ){
		return getHyper().get( url = "/company/stock/list" );
	}

	public function getMajorIndex( string ticker = "" ){
		var endpoint = "/majors-indexes";
		if ( arguments.ticker > "" ){
			endpoint &= "/#arguments.ticker#";
		}
		return getHyper().get( url = endpoint );
	}

	public function getActiveStocks(){
		return getHyper().get( url = "/stock/actives" );
	}

	public function getMostGainedStocks( ){
		return getHyper().get( url = "/stock/gainers" );
	}

	public function getMostLostStocks(){
		return getHyper().get( url = "/stock/losers" );
	}

	public function getNYSEMarketHours(){
		return getHyper().get( url = "/is-the-market-open" );
	}

	public function getSectorPerformance(){
		return getHyper().get( url = "/stock/sectors-performance" );
	}

	public function getIncomeStatement( required string ticker, string period = "annual" ){
		var endpoint = "/financials/income-statement/" & arguments.ticker;
		var validPeriods = ["annual","quarter"];
		if ( !validPeriods.find( arguments.period ) ){
			throw( "Invalid period; must be one of #validPeriods.toList()#" );
		}
		var params = {
			"period" : arguments.period
		};
		return getHyper().get( url = endpoint, queryParams = params );
	}

	public function getBalanceSheet( required string ticker, string period = "annual" ){
		var endpoint = "/financials/balance-sheet-statement/" & arguments.ticker;
		var validPeriods = ["annual","quarter"];
		if ( !validPeriods.find( arguments.period ) ){
			throw( "Invalid period; must be one of #validPeriods.toList()#" );
		}
		var params = {
			"period" : arguments.period
		};
		return getHyper().get( url = endpoint, queryParams = params );
	}

	public function getEnterpriseValue( required string ticker, string period = "annual" ){
		var endpoint = "/enterprise-value/" & arguments.ticker;
		var validPeriods = ["annual","quarter"];
		if ( !validPeriods.find( arguments.period ) ){
			throw( "Invalid period; must be one of #validPeriods.toList()#" );
		}
		var params = {
			"period" : arguments.period
		};
		return getHyper().get( url = endpoint, queryParams = params );
	}

	public function getKeyMetrics( required string ticker, string period = "annual" ){
		var endpoint = "/company-key-metrics/" & arguments.ticker;
		var validPeriods = ["annual","quarter"];
		if ( !validPeriods.find( arguments.period ) ){
			throw( "Invalid period; must be one of #validPeriods.toList()#" );
		}
		var params = {
			"period" : arguments.period
		};
		return getHyper().get( url = endpoint, queryParams = params );
	}

	public function getFinancialGrowth( required string ticker, string period = "annual" ){
		var endpoint = "/financial-statement-growth/" & arguments.ticker;
		var validPeriods = ["annual","quarter"];
		if ( !validPeriods.find( arguments.period ) ){
			throw( "Invalid period; must be one of #validPeriods.toList()#" );
		}
		var params = {
			"period" : arguments.period
		};
		return getHyper().get( url = endpoint, queryParams = params );
	}

}