/**
 * Handle generic API stuff like JSON parsing and error handling
*/
component accessors="true"{
	/**
	 * The base URL of the API, like http://financialmodelingprep.com/api/vx
	*/
	property name="baseURL" inject="box:setting:baseURL@cfstocky";
	/**
	 * The hyper request builder
	 * https://github.com/coldbox-modules/hyper
	*/
	property name="hyper" inject="provider:HyperRequest@Hyper";

	public BaseAPI function init(){
		return this;
	}

	public function getHyper(){
		return hyper.setBaseUrl( getBaseURL() );
	}
}